import React, {Component} from 'react';
import { Container, Row, Col, Card, CardImg, CardText, CardBody,
    CardTitle, Button
  } from 'reactstrap';  
import axios from 'axios';

class ArtikelApp extends Component{
    constructor(){
        super();
        this.state = {
            artikels: []
        }
    }
    componentDidMount(){
        axios.get(`http://reduxblog.herokuapp.com/api/posts?key=poro`)
            .then(response => {
                this.setState({
                    artikels: response.data
                })
            })
    }

    handleDelete = artikelId=> {
        const artikelupdate = [this.state.artikels];
        axios.delete(`http://reduxblog.herokuapp.com/api/posts/${artikelId}`)
            .then(res => {
                console.log(res);
                alert('removed');
                this.setState({artikels:artikelupdate});
                window.location.reload();
            })
            .catch(err => {
                console.log(err)
            })
    }
    render(){
        return(
            <div>
                <Container>
                 <Row>
                        {
                            this.state.artikels.map((artikel, index) =>
                                <Col key={index} md='4'>
                                    <Card>
                                        <CardImg top width="100%" src={artikel.categories} />
                                        <CardBody>
                                        <CardTitle>{artikel.title}</CardTitle>
                                        <CardText>{artikel.content}</CardText>
                                        <Button color='danger' onClick={(e) => this.handleDelete(artikel.id)}>
                                            Delete
                                        </Button>
                                        </CardBody>
                                    </Card>
                                   
                                </Col>    
                            )
                        }
                    </Row>
                    </Container>
                    </div>
        )
    }
}
export default ArtikelApp;
