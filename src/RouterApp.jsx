import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Home from './Home';
import AdminApp from './Page/AdminPage';


class Router extends Component{
    render(){
        return(
        <div>
            <BrowserRouter>
                <div>
                    <Route exact path='/' component={Home} />
                    <Route path = '/admin' component={AdminApp} />
                </div>
            </BrowserRouter>
        </div>
        )
    }
}

export default Router;