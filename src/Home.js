import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import HeaderApp from './Component/HeaderApp';
import {Container,Row,Col} from 'reactstrap';
import ArtikelApp from './Component/Artikel';
import SlideApp from './Component/Slide';
class Home extends Component {
  render() {
    return (
     <div>
       
       <Container>
         <Row>
         <Col>
            <HeaderApp />
            </Col>
            </Row>
        </Container>
        <div className='Body'>
        <Container>
        <SlideApp />
        <Row>
          <ArtikelApp />
        </Row>
        </Container>
        </div>
     </div>
    );
  }
}

export default Home;
