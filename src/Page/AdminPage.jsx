import React, {Component} from 'react';
import HeaderApp from '../Component/HeaderApp';
import ImputApp from '../Component/Imput';
import {Container,Row,Col} from 'reactstrap';


class AdminApp extends Component{
    render(){
        return(
            <div>
                <Container>
                        <HeaderApp />
                    <Row>
                        <Col>
                        <ImputApp />
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}
export default AdminApp;